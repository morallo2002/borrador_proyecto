const express = require('express');
const app = express();
app.use(express.json());

const port = process.env.PORT || 3000;
app.listen(port);

console.log("API escuchando en el puerto BIP BIP: " + port);

app.get("/apitechu/v1/hello",
  function(req, res){
    console.log("GET /apitechu/v1/hello");

    res.send({"msg" : "Hola desde API TechU"});
  }
)

app.get("/apitechu/v1/users",
  function(req, res){
    console.log("GET /apitechu/v1/users");
    // res.sendFile('usuarios.json', {root: __dirname});
    var users = require('./usuarios.json');
    //var top = req.query.$top;

    var objeto = {};

    objeto.users = req.query.$top ? users.slice(0, req.query.$top) : users;

    var count = req.query.$count;
    if (count == "true") {
      objeto.count = users.length;
    }

    res.send(objeto);
  }
)

app.post("/apitechu/v1/login",
  function(req, res){
      console.log("POST /apitechu/v1/login POST.......");

      console.log("Body:");
      console.log(req.body);

      var users = require("./usuariosPractica.json");
      var found = false;
      var pwdFound = false;

      // ======================================================================
      // Búsqueda por método FOR
      var auxUser;

      for (var i = 0; ((i < users.length) && (!found)); i++) {
        auxUser = users[i];

        if (auxUser.email == req.body.email) {
          found = true;
          console.log("ENCONTRADO email!!!" + req.body.email);
          if (auxUser.pwd == req.body.password) {
            console.log("ENCONTRADO pwd!!!" + req.body.password);
            pwdFound = true;
            users[i].logged = true;
            writeUserDataToFile(users);
          }
        }
      }

      var msg = pwdFound ? "Login correcto" : "Login incorrecto";
      console.log(msg);
      if (pwdFound) {
          res.send({"msg" : msg, "idUsuario" : auxUser.id});
      } else {
          res.send({"msg" : msg});
      }

  }
)

app.post("/apitechu/v1/logout",
  function(req, res){
      console.log("POST /apitechu/v1/logout POST.......");

      console.log("Body:");
      console.log(req.body);

      var users = require("./usuariosPractica.json");
      var found = false;
      var logout = false;

      // ======================================================================
      // Búsqueda por método FOR
      var auxUser;

      for (var i = 0; ((i < users.length) && (!found)); i++) {
        auxUser = users[i];

        if (auxUser.id == req.body.id) {
          found = true;
          console.log("ENCONTRADO id!!!" + req.body.id);
          if (auxUser.logged == true) {
            console.log("LOGADO!!!");
            logout = true;
            delete users[i].logged;
            writeUserDataToFile(users);
          }
        }
      }

      var msg = logout ? "logout correcto" : "logout incorrecto";
      console.log(msg);
      if (logout) {
          res.send({"msg" : msg, "idUsuario" : auxUser.id});
      } else {
          res.send({"msg" : msg});
      }

  }
)

app.post("/apitechu/v1/users",
  function(req, res){
    console.log("GET /apitechu/v1/users POST.......");

    console.log(req.headers.first_name);
    console.log(req.headers.last_name);
    console.log(req.headers.email);

    var newUser = {
      "first_name" : req.headers.first_name,
      "last_name" : req.headers.last_name,
      "email" : req.headers.email
    };


    var users = require('./usuarios.json');
    users.push(newUser);
    // escribe en memoria, pero no físicamente en fichero. El require carga copia en memoria

    writeUserDataToFile(users);

    res.send("Usuario añadido con éxito");
  }
)

app.delete("/apitechu/v1/users/:id",
  function(req, res){
    console.log("DELETE /apitechu/v1/users/:id");
    console.log("La id enviada es: " + req.params.id);

    var users = require("./usuarios.json");
    //users.splice(req.params.id - 1, 1);
    var found = false;

    // ======================================================================
    // Búsqueda por método FOR
    var auxUser;

    for (var i = 0; ((i < users.length) && (!found)); i++) {
      auxUser = users[i];

      if (auxUser.id == req.params.id) {
        found = true;
        users.splice(i, 1);
      }
    }


    // ======================================================================
    // Búsqueda por método FOR IN
    // for(var i in users){
    //   auxUser = users[i];
    //
    //   if (auxUser.id == req.params.id) {
    //     found = true;
    //     users.splice(i, 1);
    //     break;
    //   }
    // }


    if (found) {
      writeUserDataToFile(users);
    }

    var msg = found ? "Usuario borrado" : "Usuario no encontrado";
    console.log(msg);
    res.send({"msg" : msg});
  }
)

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res){
    console.log("GET /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parámetros:");
    console.log(req.params);

    console.log("Query string:");
    console.log(req.query);

    console.log("Headers:");
    console.log(req.headers);

    console.log("Body:");
    console.log(req.body);
  }
)

function writeUserDataToFile(data){
  const fs = require('fs');

  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuariosPractica.json", jsonUserData, "utf8",
    function(err) {
        if (err) {
          console.log(err);
        } else {
          console.log("Datos escritos en el fichero");
        }
    }
  );
}
